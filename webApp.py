#!/usr/bin/env python3

import socket

class webApp:

    def __init__(self,hostname,port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        mySocket.listen(5)

        try:
            while True:
                print("Waiting for connections")
                (recvSocket, address) = mySocket.accept()
                print("HTTP request received (going to parse and process):")
                request = recvSocket.recv(2048)
                print(request)
                parsedRequest = self.parse(request.decode('utf8'))
                (returnCode, htmlAnswer) = self.process(parsedRequest)
                print("Answering back...")
                response = "HTTP/1.1 " + returnCode + " \r\n\r\n" \
                           + htmlAnswer + "\r\n"
                recvSocket.send(response.encode('utf8'))
                recvSocket.close()
        except KeyboardInterrupt:
            socket.close()

if __name__ == "__main__":
    testWebApp = webApp("localhost",1234)


